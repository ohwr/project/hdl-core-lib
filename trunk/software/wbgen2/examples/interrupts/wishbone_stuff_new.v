
// wishbone testbench utilities
begin : wb
reg [31:0] wb_addr = 0, wb_data_o = 0;
   reg [3:0] wb_bwsel =4'b1111;
   wire [31:0] wb_data_i;
   wire wb_ack;
   reg wb_cyc=0, wb_stb=0, wb_we= 0;
   reg wb_tb_verbose = 1;
   time last_access_t = 0;


   task verbose;
      input onoff;
      begin
	 wb_tb_verbose = onoff;
      end
   endtask // wb_verbose

      

   task rw_generic;
      input[31:0] addr;
      input [31:0] data_i;
      output [31:0] data_o;
      input rw;
      input [3:0] size;
      begin
	 
	 if(wb_tb_verbose) $display("WB write: addr %x, data %x", addr, data);

	 if($time != last_access_t) begin
	    @(posedge clk) #0;
	 end
	 	 
	 wb_stb=1;
	 wb_cyc=1;
	 wb_addr = {2'b00, addr[31:2]};
	 wb_we = rw;
	 wb_bwsel = bytesel;

	 case(size)
	   4: wb_data_o=data_i;
	   2: begin
	      
	      if(addr[1]) begin
		 wb_data_o[31:16] = data_i[15:0];
		 wb_bwsel = 4'b1100;
	      else
		wb_data_o[15:0] = data_i[15:0];
		 wb_bwsel = 4'b0011;
	      end
	   end
	   1: begin
	      case(addr[1:0])
		0: wb_data_o[7:0] = data_i[7:0]; wb_bwsel = 4'b0001;
		1: wb_data_o[7:0] = data_i[7:0]; wb_bwsel = 4'b0001;
		2: wb_data_o[7:0] = data_i[7:0]; wb_bwsel = 4'b0001;
	        3: wb_data_o[7:0] = data_i[7:0]; wb_bwsel = 4'b0001;
		    

	      endcase // case(addr[1:0])

	   end

	   default: $error("Invalid operation size: ", size);
	 endcase // case(size)
	 
      

	 #(`wbclk_period-1);
	 
	 if(wb_ack == 0) begin
	    while(wb_ack == 0) begin @(posedge clk); #0; end
	 end

	 data_o = wb_data_i;
 	 wb_cyc = 0;
	 wb_we=0;
	 wb_stb=0;

	 last_access_t = $time;
      end
   endtask // rw_generic

  task wb_write_byte;
   input[31:0] addr;
   input [31:0] data;
      begin
	 if(wb_tb_verbose) $display("WB write_byte: addr %x, data %x", addr, data);


	 	 
	 wb_stb=1;
	 wb_cyc=1;
	 wb_addr ={ 2'b00, addr[31:2] };
	 wb_data_o= (addr [1:0] == 2'b00) ? {data[7:0], 24'bx} :
		    (addr [1:0] == 2'b01) ? {8'bx, data[7:0], 16'bx} :
		    (addr [1:0] == 2'b10) ? {16'bx, data[7:0], 8'bx} :
		    (addr [1:0] == 2'b11) ? {24'bx, data[7:0]} : 32'bx;
	 
	 wb_we = 1;
	 wb_bwsel = (addr [1:0] == 2'b00) ? 'b1000 :
		    (addr [1:0] == 2'b01) ? 'b0100 :
		    (addr [1:0] == 2'b10) ? 'b0010 :
		    (addr [1:0] == 2'b11) ? 'b0001 : 4'bxxxx;
	 
 
	 while(wb_ack == 0) begin @(posedge clk); #1; end
	 

 	 @(posedge clk); #1;
 	 wb_cyc = 0;
	 wb_we=0;
	 wb_stb=0;

      end
   endtask // wb_write


   task wb_read;
      input[31:0] addr;
      output [31:0] data;
      begin
	 wb_bwsel=4'hf;
	 wb_stb=1;
	 wb_cyc=1;
	 wb_addr = {2'b00, addr[31:2]};
	 wb_data_o=data;
	 wb_we = 0;

	 while(wb_ack == 0) @(posedge clk);
	 #1 data = wb_data_i;

	 
//	 @(posedge clk);
	 wb_cyc = 0;
	 wb_we=0;
	 wb_stb=0;
	 if(wb_tb_verbose) $display("WB read: addr %x data %x", addr, data);
 

      end
   endtask // wb_read

  task wb_read_byte;
      input[31:0] addr;
      output [31:0] data;
      
      begin : task_wb_read_byte
	reg [31:0] data_tmp;
	 wb_bwsel=4'hf;
	 wb_stb=1;
	 wb_cyc=1;
	 wb_addr = {2'b00, addr[31:2]};
	 wb_data_o=data;
	 wb_we = 0;

	 while(wb_ack == 0) @(posedge clk);
	 #1 data = (addr [1:0] == 2'b00) ? wb_data_i[31:24] :
		    (addr [1:0] == 2'b01) ? wb_data_i[23:16] :
		    (addr [1:0] == 2'b10) ? wb_data_i[15:8] :
		    (addr [1:0] == 2'b11) ? wb_data_i[7:0] : 4'bxxxx;
	 
	 
	 @(posedge clk);
	 wb_cyc = 0;
	 wb_we=0;
	 wb_stb=0;
	 if(wb_tb_verbose) $display("WB read byte: addr %x data %x", addr, data);
 

      end
   endtask // wb_read
end