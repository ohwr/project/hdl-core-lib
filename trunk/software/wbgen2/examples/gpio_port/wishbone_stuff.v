
   reg [31:0] wb_addr, wb_data_o, tmp;
   wire [31:0] wb_data_i;
   wire wb_ack;
   reg wb_sel =0, wb_cyc=0, wb_stb=0, wb_we= 0;

   task delay_cycles;
      input [31:0] n;
      begin
	 		#(n * `wbclk_period);
      end
   endtask // delay_cycles

	 task wb_write;
      input[31:0] addr;
      input [31:0] data;
      begin
      $display("WB write: addr %x, data %x", addr, data);
      wb_sel=1;
      wb_stb=1;
      wb_cyc=1;
      wb_addr = addr;
      wb_data_o=data;
      wb_we = 1;
 
      delay_cycles(1);
      while(wb_ack == 0)
				delay_cycles(1);
				
			delay_cycles(1);
      wb_cyc = 0;
      wb_sel=0;
      wb_we=0;
      wb_stb=0;
      end
   endtask // wb_write


   task wb_read;
      input[31:0] addr;
      output [31:0] data;
      begin
      wb_sel=1;
      wb_stb=1;
      wb_cyc=1;
      wb_addr = addr;
      wb_data_o=data;
      wb_we = 0;
	 
      delay_cycles(1);
      while(wb_ack == 0)
				delay_cycles(1);

      data = wb_data_i;
	
			delay_cycles(1);
      wb_cyc = 0;
      wb_sel=0;
      wb_we=0;
      wb_stb=0;
      end
   endtask // wb_read

