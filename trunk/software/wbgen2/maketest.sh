#!/bin/bash

./wbgen2.lua regs_with_fields.wb -vo a.vhd -co a.h
#vlib work
#vlib wbgen2
#vcom -work wbgen2 lib/wbgen2_pkg.vhd
#vcom -work wbgen2 lib/wbgen2_dpssram.vhd
#vcom a.vhd
