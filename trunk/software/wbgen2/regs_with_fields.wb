-- here comes our peripheral definition
peripheral {
-- short (human-readable) name for the peripheral.
	name = "Test fields and regs";
-- a longer description, if you want
	description = "";
-- name of the target VHDL entity to be generated
	hdl_entity = "wb_slave_test_regs_fields";

-- prefix for all the generated ports belonging to our peripheral
	prefix = "TESTRF";

-- Pin direction register. Readable and writable from the bus, readable from the device.
	reg {
		name = "Test register SLV/BITs";
		description = "Test register SLV/BITs";
		prefix = "TRSLV";

		field {
			name = "Bit field 1";
			description = "";
			prefix = "BIT1";
			type = BIT;
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};

		field {
			name = "Bit field 2";
			description = "";
			prefix = "BIT2";
			type = BIT;
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};

		field {
			name = "Reset bit";
			description = "write 1 to reset something";
			prefix = "RESET";
			type = MONOSTABLE;
		};

		field {
			name = "Counter";
			description = "8-bit counter";
			prefix = "CNTR";
			type = SLV;
			size = 8;
			align = 8;
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};

		field {
			name = "16-bit value";
			description = "16-bit SLV";
			prefix = "SLV16";
			type = SLV;
			size = 16;
			align = 16;
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};

	};

	reg {
		name = "Test register signed";
		prefix = "TSIGNED";
		
		field {
			name = "Signed with range";
			prefix = "signedrange";
			type = SIGNED;
			range = {-100, 100};
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};

		field {
			name = "Signed 1";
			prefix = "signed12bit";
			type = SIGNED;
			align = 16;
			size = 12;
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};
		
	};
	

	reg {
		name = "Test register UNsigned";
		prefix = "TUNSIGNED";
		
		align = 4;
		
		field {
			name = "UnSigned with range";
			prefix = "unsrange";
			type = UNSIGNED;
			range = {-200, 250};
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};

		field {
			name = "UNSigned 1";
			prefix = "signed13bit";
			type = UNSIGNED;
			align = 16;
			size = 13;
			access_bus = READ_WRITE;
			access_dev = READ_ONLY;
		};
		
	};

		ram {
		name = "memory 1";
		prefix = "mem1K";
		size = 256;
		width = 32;
		byte_select = true;
		clock = "clk1_i";
		wrap_bits = 1;
		access_bus = READ_WRITE;
		access_dev = READ_WRITE;
	};

	ram {
		name = "memory 2";
		prefix = "mem2K";
		size = 1024;
		width = 16;
		access_bus = READ_WRITE;
		access_dev = READ_ONLY;
	};


};
