-- -*- Mode: LUA; tab-width: 2 -*-

-- wbgen2 - a simple Wishbone slave generator
-- (c) 2010 Tomasz Wlostowski
-- CERN BE-Co-HT
-- LICENSED UNDER GPL v2

-- EIC (tm) = Embedded Interrupt Controller
-- regs: 
--
-- EIC_IER = interrupt enable reg [passthru]
-- EIC_IDR = interrupt disable reg [passthru]
-- EIC_IMR = interrupt mask reg [rw, load-ext]
-- EIC_ISR = interrupt status reg [rw, reset on write 1]
--

function wbgen_gen_irq_controller()
-- trigger = IRQ_POSEDGE, NEGEDGE, HIGH, LOW
-- name
-- prefix

end
